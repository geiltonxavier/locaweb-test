require 'rails_helper'
require_relative './../../app/services/twitter_service.rb'
require 'webmock/rspec'

RSpec.describe 'Locaweb Tweets' do
  let (:tweets) { tweets = TwitterService.new }
  before(:each) { stubs }
  describe 'most relevants' do
    it 'list most relevants' do
      most_relevants = JSON tweets.most_relevants
      expect(most_relevants.count).to eq(10)
    end
  end

  describe 'most mentions' do
    it 'list most mentions' do
      most_mentions = JSON tweets.most_mentions
      expect(most_mentions.keys).to eq(["lorna_connelly", "bode_miss_jamar", "hara_immanuel_o", "loma_grady", "reichel_richard", "stracke_bulah", "nitzsche_tobin", "raymond_hilll", "jocelyn_iv_konopelski", "sawayn_lester_md"])
    end
  end

  def stubs
    response = File.open("#{Dir.pwd}/spec/response.json").read
    stub_request(:get, 'http://tweeps.locaweb.com.br/tweeps').with(headers: { 'Username' => 'geilton@live.com' }).to_return(body: response)

  end
end
