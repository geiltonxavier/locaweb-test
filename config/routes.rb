Rails.application.routes.draw do
  root to: "home#index"

    get "/most_relevants" => "twitter#most_relevants"

    get "/most_mentions" => "twitter#most_mentions"

  
  namespace :api, defaults: {format: :json} do
    namespace :v1 do  
       get "/most_mentions" => "twitter#most_mentions"
       get "/most_relevants" => "twitter#most_relevants"
    end

end
end
