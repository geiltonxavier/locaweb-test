## Dependências

- Ruby 2.3.4
- Rails 5.1.2
- SQLite 3

## Instaling

Realize o clone!
```console
git clone git@bitbucket.org:geiltonxavier/locaweb-test.git
cd locaweb-test
bundle install
```


Instalação da Base de Dados

```no console execute
bundle exec rake db:setup
bundle exec rake db:migrate
bundle exec rake db:test:prepare
```

## Running

Dentro da pasta do projeto:

```no console execute
rails server
```
Acesse o servidor local através da URL [http://localhost:3000](http://localhost:3000)

Acesso a os recursos **em formato JSON**, nas seguintes
URIs:

api/v1/most_relevants
    Mostra os tweets mais relevantes
    
api/v1//most_mentions
    Mostra os usuários que mais mencionaram a locaweb

## Tests

Dentro da pasta do projeto:

```console
bundle exec rspec
```