class TwitterController < ApplicationController

    def most_relevants
        @most_relevants = JSON.parse(TwitterService.new.most_relevants)
    end

    def most_mentions
        @most_mentions  = JSON.parse(TwitterService.new.most_mentions)
    end

end
