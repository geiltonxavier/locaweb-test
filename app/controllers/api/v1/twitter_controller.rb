module Api
    module V1
      class TwitterController < ApplicationController
    
            def most_relevants
                @most_relevants = TwitterService.new.most_relevants
                render json: @most_relevants
            end

            def most_mentions
                @most_mentions  = TwitterService.new.most_mentions
                render json: @most_mentions
            end

      end
    end
end