require 'uri'
require 'net/http'
require 'json'

class TwitterService

LOCAWEB_TWEEPS_URL = URI('http://tweeps.locaweb.com.br/tweeps'.freeze)
 
 def initialize(http_service = HttpService)
    @http_service = http_service
 end


  def most_relevants
    JSON sort(find_all_tweets_by_user_id).map { |tweet| fields(tweet) }
  end

  def most_mentions
      mentions = sort(find_all_tweets_by_user_id).group_by do |tweet|
        tweet['user']['screen_name']
      end

      mentions.each do |key, value|
        mentions[key] = sort(value).map! { |tweet| fields(tweet) }
      end
      JSON mentions
  end

  def find_all_tweets_by_user_id(user_id = 42)
    fetch_tweets['statuses'].select do |tweet|
        reply_to_user_id = !tweet['in_reply_to_user_id'].eql?(user_id)
        user_mentions = tweet['entities']['user_mentions'].any? do |mention|
          mention['id'].eql?(user_id)
        end
        reply_to_user_id && user_mentions
      end
  end

protected

  def fetch_tweets
    tweets = @http_service.get(LOCAWEB_TWEEPS_URL,{'Username' => 'geilton@live.com'})
    tweets
  end

  def fields(tweet)
      result = {}
      result[:user] = {}
      result[:user][:profile] = {
        screen_name: tweet['user']['screen_name'],
        url: tweet['user']['profile_image_url']
      }
      result[:user][:info] = {
        followers_count: tweet['user']['followers_count']
      }
      result[:tweet] = {
        content: tweet['text'],
        date: tweet['created_at'],
        retweet_count: tweet['retweet_count'],
        favorite_count: tweet['favorite_count']
      }
      result
    end

    def sort(tweets)
      tweets.sort_by do |tweet|
        [
          (-tweet['user']['followers_count']),
          (-tweet['retweet_count']),
          (-tweet['favorite_count'])
        ]
      end
    end
end