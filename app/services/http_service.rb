class HttpService
  def self.get(uri, params = {})
    request = Net::HTTP::Get.new(uri)
      request.initialize_http_header(params)
      response = Net::HTTP.start(uri.hostname, uri.port) do |http|
        http.request(request)
      end
      result = JSON.parse(response.body)
      result
  end
end